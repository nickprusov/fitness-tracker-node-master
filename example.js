// initialize the express application
var express = require("express"),
    app = express();
var FitnessTrackerApiClient = require('../fitness-tracker-node-master');

// initialize the Fitbit API client
//var FitnessTrackerApiClient = require("fitness-tracker-node"),

//var client = new FitnessTrackerApiClient("vqkRNxRurRU", "cce8901140e6575fe096c55727cc0bc4a639065a",'https://jawbone.com','/auth/oauth2/auth','/auth/oauth2/token' );
//var client = new FitnessTrackerApiClient("227V2W", "6bdd6746df4c789e7c1be5b8cf7a484f",'https://api.fitbit.com/','https://www.fitbit.com/oauth2/authorize','oauth2/token' );
//var client = new FitnessTrackerApiClient("rn1GL52W04Yx_J10SW4m9armKBiYNLHR", "6a5628m8tvW65xSRdq2tBDNSXC19fRM6cR2bQG3xViOLWiGwOgFOU29Vv9j5N3FU",'https://api.moves-app.com/'
//    ,'oauth/v1/authorize','oauth/v1/access_token' );
//var client = new FitnessTrackerApiClient("93d8c21788ea4a41965ef6177e0019c8", "d951c90a5e7a4aa5b48ff6680e08b465",'https://runkeeper.com/','apps/authorize','apps/token' );

var client = new FitnessTrackerApiClient({
    withings: true,
    clientID: "f5263356ed50d69c05024877c91dec594d4f066964bce03e3c72981fb50",
    clientSecret: "6c04b8622c3742720ec2b8b530f385ed3bf320ce4b29f41f11996d14853"
});

var client2 = new FitnessTrackerApiClient({
    clientID: "227V2W",
    clientSecret: "6bdd6746df4c789e7c1be5b8cf7a484f",
    site: 'https://api.fitbit.com/',
    authorizationPath: 'https://www.fitbit.com/oauth2/authorize',
    tokenPath: 'oauth2/token'
});


// redirect the user to the Fitbit authorization page
app.get("/api/authorize", function (req, res) {
    // request access to the user's activity, heartrate, location, nutrion, profile, settings, sleep, social, and weight scopes
    //res.redirect(client.getAuthorizeUrl('basic_read extended_read location_read friends_read mood_read mood_write move_read move_write sleep_read sleep_write meal_read meal_write weight_read weight_write generic_event_read generic_event_write heartrate_read', 'http://localhost:3000/api/callback'));
    client.getAuthorizeUrl('activity heartrate location nutrition profile settings sleep social weight', 'http://localhost:3000/api/callback').then(function (result) {
        res.redirect(result);
        //res.redirect(client.getAuthorizeUrl('default activity location', 'http://localhost:3000/api/callback'));
        //res.redirect(client.getAuthorizeUrl('https://www.googleapis.com/auth/fitness.activity.write', 'http://localhost:3000/api/callback'));
    }).catch(function (error) {
        res.send(error);
    });
});
// handle the callback from the Fitbit authorization flow
app.get("/api/callback", function (req, res) {
    // exchange the authorization code we just received for an access token
    client.getAccessToken(req.query.code ? req.query.code : req.query.oauth_verifier, 'http://localhost:3000/api/callback', req.query.userid).then(function (result) {
        res.json(result);
    }).catch(function (error) {
        res.send(error);
    });
});

// launch the server
app.listen(3000);