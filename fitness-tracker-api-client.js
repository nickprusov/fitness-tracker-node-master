var OAuth2 = require('simple-oauth2'),
    Q = require('q');
const Withings = require('withings-oauth2').Withings;

function FitnessTrackerApiClient(options) {
    this.options = options;
    if (!this.options.withings) {
        this.oauth2 = OAuth2({
            clientID: options.clientID,
            clientSecret: options.clientSecret,
            site: options.site,
            authorizationPath: options.authorizationPath,
            tokenPath: options.tokenPath,
            useBasicAuthorizationHeader: true
        });
    }
}

FitnessTrackerApiClient.prototype = {
    getAuthorizeUrl: function (scope, redirectUrl) {
        var deferred = Q.defer();
        if (!this.options.withings) {
            deferred.resolve(this.oauth2.authCode.authorizeURL({
                scope: scope,
                redirect_uri: redirectUrl,
                access_type: 'offline'
            }));
        } else {
            var those = this;
            const config = {
                consumerKey: this.options.clientID,
                consumerSecret: this.options.clientSecret,
                callbackUrl: redirectUrl
            };
            var withingsClient = new Withings(config);
            withingsClient.getRequestToken(function (err, token, tokenSecret) {
                if (err) {
                    deferred.reject(err);
                }
                those.oauth = {
                    requestToken: token,
                    requestTokenSecret: tokenSecret
                };
                deferred.resolve(withingsClient.authorizeUrl(token, tokenSecret));
            });
        }
        return deferred.promise;
    },

    getAccessToken: function (code, redirectUrl, userId) {
        var deferred = Q.defer();
        if (!this.options.withings) {
            this.oauth2.authCode.getToken({
                code: code,
                redirect_uri: redirectUrl
            }, function (error, result) {
                if (error) {
                    deferred.reject(error);
                } else {
                    deferred.resolve({
                        authToken: code,
                        accessToken: result.access_token,
                        refreshToken: result.refresh_token
                    });
                }
            });
        } else {
            const verifier = code;
            const config = {
                consumerKey: this.options.clientID,
                consumerSecret: this.options.clientSecret,
                callbackUrl: redirectUrl,
                userID: userId
            };
            var withingsClient = new Withings(config);
            withingsClient.getAccessToken(this.oauth.requestToken, this.oauth.requestTokenSecret,
                verifier, function (err, token, secret) {
                    if (err) {
                        deferred.reject(err);
                    }
                    deferred.resolve({
                        authToken: code,
                        accessToken: token,
                        accessTokenSecret: secret
                    });
                });
        }
        return deferred.promise;
    },

    refreshAccesstoken: function (accessToken, refreshToken, expiresIn) {
        if (expiresIn === undefined) expiresIn = -1;

        var deferred = Q.defer();

        var token = this.oauth2.accessToken.create({
            access_token: accessToken,
            refresh_token: refreshToken,
            expires_in: expiresIn
        });

        token.refresh(function (error, result) {
            if (error) {
                deferred.reject(error);
            } else {
                deferred.resolve(result.token);
            }
        });

        return deferred.promise;
    }
};

module.exports = FitnessTrackerApiClient;
